﻿using UnityEngine;
using System.Collections;
using SWS;
using UnityEngine.Events;
using DG.Tweening;

public class TeacherMovement : MonoBehaviour {

    public float Speed;
    public PathManager PathContainer { get; set; }
    public bool PathRandom { get; set; }
    public int Health { get; set;  }

    private Animator _animator;
    private float _speed;
    private splineMove _splineMove;

    public void Hit(int damage, string name)
    {
        _animator.Play("HitHead");
        Health -= damage;

        if (Health <= 0)
        {
            _splineMove.Stop();
            Instantiate(GameManager.Instance.EffectFactory.EffectGush, transform.position + Vector3.up, Quaternion.identity);
            Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position + Vector3.up, Quaternion.identity);
            Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position + Vector3.up, Quaternion.identity);
            Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position + Vector3.up, Quaternion.identity);
            gameObject.transform.position = Vector3.zero;
            _splineMove.GoToWaypoint(0);
            GameManager.Instance.GameController.TeacherDestroyed();
            GameManager.Instance.SoundController.PlayYes();
            StartLevel();
        }
    }

    private IEnumerator delay()
    {
        yield return new WaitForSeconds(Random.Range(10, 30));
        GameManager.Instance.SoundController.PlayTeacher();
        StartMoving();        
    }

    void StartLevel()
    {
        StartCoroutine(delay());
    }

    void EndLevel()
    {
        _splineMove.Stop();
        transform.DOMoveY(100, 10.0f);
    }

    void OnEnable()
    {
        _animator = transform.Find("Teacher").GetComponent<Animator>();
        Health = 20;
    }

    void StartMoving()
    {
        D.Log("- PathContainer = {0}", PathContainer);

        if (PathContainer != null)
        {
            _splineMove = GetComponent<splineMove>();
            _splineMove.pathContainer = PathContainer;
            if (PathRandom)
            {
                _splineMove.loopType = splineMove.LoopType.random;
            }

            _splineMove.loopType = splineMove.LoopType.none;
            _speed = Speed + GameManager.Instance.Level;
            _splineMove.speed = _speed;
            UnityEvent e = new UnityEvent();
            e.AddListener(waypointEvent);
            _splineMove.events.Add(e);
            _splineMove.StartMove();

        }
    }

    private void waypointEvent()
    {
        D.Log("- teacher reached a waypoint");
    }

}
