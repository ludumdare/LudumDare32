﻿using UnityEngine;
using System.Collections;

public class EnemyFacing : MonoBehaviour 
{

	// Use this for initialization
	void Start () {
	
	}

    // 	determine which cell has been touched
    void LateUpdate()
    {
            Vector3 _lookPos = new Vector3(GameManager.Instance.PlayerPosition.x, transform.position.y, GameManager.Instance.PlayerPosition.z);
            transform.LookAt(_lookPos);
    }
}
