﻿using UnityEngine;
using System.Collections;

public class EffectFactory : MonoBehaviour 
{
    public GameObject EffectGibs;
    public GameObject EffectSplat;
    public GameObject EffectGush;

    void OnEnable()
    {
        GameManager.Instance.EffectFactory = this;
    }

}
