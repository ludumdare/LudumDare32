﻿using UnityEngine;
using System.Collections;

public class FoodObject : MonoBehaviour 
{
    public string Name;
    public int Damage;
    public float Accuracy;
    public float Velocity;
    public float Lift;

    public virtual void OnCreated()
    {
        D.Log("[FoodObject] OnCreated");
    }

    public virtual void OnDestroyed()
    {
        D.Log("[FoodObject] OnDestroyed");
    }

    public virtual void OnCollideWall()
    {
        D.Log("[FoodObject] OnCollidedWall");
        Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public virtual void OnCollideEnemy(EnemyController e)
    {
        D.Log("[FoodObject] OnCollidedEnemy");
        if (Random.Range(0,20) == 0) GameManager.Instance.SoundController.PlayUgh();
        transform.parent = e.transform;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().useGravity = false;
        Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position, Quaternion.identity);
        GameManager.Instance.Score += 10;
        e.Hit(Damage, Name);
        //Destroy(gameObject);
    }

    public virtual void OnCollidePlayer(PlayerMovement e)
    {
        D.Log("[FoodObject] OnCollidedPlayer");
        if (Random.Range(0, 20) == 0) GameManager.Instance.SoundController.PlayOuch();
        transform.parent = e.transform;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().useGravity = false;
        Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position, Quaternion.identity);
        GameManager.Instance.Health -= 1;

    }

    public virtual void OnCollideTeacher(TeacherMovement e)
    {
        D.Log("[FoodObject] OnCollidedTeacher");
        transform.parent = e.transform;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().useGravity = false;
        Instantiate(GameManager.Instance.EffectFactory.EffectGibs, transform.position, Quaternion.identity);
        GameManager.Instance.Score += 100;
        e.Hit(Damage, Name);
    }

	// Use this for initialization
	public virtual void OnFired()
    {
        Vector3 fv = transform.rotation * Vector3.forward * Velocity;
        Vector3 uv = transform.rotation * Vector3.up * Lift;
        GetComponent<Rigidbody>().AddForce(fv + uv);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            PlayerMovement e = collision.transform.GetComponent<PlayerMovement>();
            OnCollidePlayer(e);
        }

        if (collision.transform.tag == "Enemy")
        {
            EnemyController e = collision.transform.GetComponent<EnemyController>();
            OnCollideEnemy(e);
        }

        if (collision.transform.tag == "Teacher")
        {
            TeacherMovement e = collision.transform.GetComponent<TeacherMovement>();
            OnCollideTeacher(e);
        }

        if (collision.transform.tag == "Wall")
        {
            OnCollideWall();
        }

        if (collision.transform.tag == "Projectile")
        {
            if (Random.Range(0, 4) == 0)
            {
                OnCollideWall();
            }
        }

        Destroy(gameObject, Random.RandomRange(1.0f, 20.0f));
    }	
}
