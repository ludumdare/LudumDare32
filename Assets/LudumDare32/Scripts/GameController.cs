﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
    public SDN.AutoHide StartPanel;
    public SDN.AutoHide WinPanel;
    public SDN.AutoHide LosePanel;

    public ArrayList Startables { get; set; }
    public int Goal { get; set;  }  //  number of teachers to eliminate

    public void TeacherDestroyed()
    {
        Goal -= 1;
        if (Goal <= 0)
        {
            //  victory
            GameManager.Instance.Level += 1;
            WinPanel.Show();
            GameManager.Instance.SoundController.PlayWinner();

            foreach (GameObject startable in Startables)
            {
                D.Warn(startable.name);
                startable.SendMessage("EndLevel", SendMessageOptions.DontRequireReceiver);
                StartPanel.Hide();
            }
        }
    }

    void OnEnable()
    {
        GameManager.Instance.GameController = this;
        Startables = new ArrayList();
        Goal = GameManager.Instance.Level;
        GameManager.Instance.Health = 100;
    }

	// Use this for initialization
	void Start () {
	
        //  show level
        GameManager.Instance.CafeteriaController.ShowCafeteria("Nixon");

        //  spawn objects

        foreach (GameObject spawner in GameObject.FindGameObjectsWithTag("Spawner"))
        {
            D.Log(spawner.name);
            spawner.SendMessage("Spawn", SendMessageOptions.DontRequireReceiver);
        }

        StartPanel.Show();
    }

    public void StartLevel()
    {
        foreach(GameObject startable in Startables)
        {
            D.Warn(startable.name);
            startable.SendMessage("StartLevel", SendMessageOptions.DontRequireReceiver);
            StartPanel.Hide();
        }
    }

    public void Quit()
    {
        Application.LoadLevel("Home");
    }

    public void Next()
    {
        Application.LoadLevel("Game");
    }
	
}
