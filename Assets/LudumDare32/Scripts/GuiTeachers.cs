﻿using UnityEngine;
using System.Collections;

public class GuiTeachers : MonoBehaviour {

    public tk2dTextMesh textMesh;

    // Update is called once per frame
    void Update()
    {
        textMesh.text = "Teachers Left: " + GameManager.Instance.GameController.Goal.ToString();
    }
}
