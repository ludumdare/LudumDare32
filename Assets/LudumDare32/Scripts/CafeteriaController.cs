﻿using UnityEngine;
using System.Collections;

public class CafeteriaController : MonoBehaviour 
{
    public CafeteriaObject[] cafeterias;

    public void ShowCafeteria(string name)
    {
        foreach(CafeteriaObject cafeteria in cafeterias)
        {
            if (cafeteria.name == name)
            {
                cafeteria.Show();
            }
        }
    }

    void OnEnable()
    {
        GameManager.Instance.CafeteriaController = this;
    }

}
