﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour 
{

	// Use this for initialization
	public void Spawn () {
        GameObject player = (GameObject) Instantiate(GameManager.Instance.PrefabFactory.PlayerPrefab, transform.position, Quaternion.identity);
        player.name = "Player";
        Destroy(gameObject);
	}
	
}
