﻿using UnityEngine;
using System.Collections;

public class PrefabFactory : MonoBehaviour 
{
    public GameObject PlayerPrefab;
    public GameObject Enemy1Prefab;
    public GameObject Enemy2Prefab;
    public GameObject Teacher1Prefab;
    public GameObject Teacher2Prefab;

    public GameObject GetRandomEnemy()
    {
        if (Random.Range(0,2) == 0)
        {
            return Enemy1Prefab;
        }
        else
        {
            return Enemy2Prefab;
        }
    }

    public GameObject GetRandomTeacher()
    {
        if (Random.Range(0, 2) == 0)
        {
            return Teacher1Prefab;
        }
        else
        {
            return Teacher2Prefab;
        }
        
    }
    void OnEnable()
    {
        GameManager.Instance.PrefabFactory = this;
    }
}
