﻿using UnityEngine;
using System.Collections;
using SWS;

public class TeacherSpawner : MonoBehaviour {

    public PathManager PathContainer;

    public void Spawn()
    {
        GameObject enemy = (GameObject)Instantiate(GameManager.Instance.PrefabFactory.GetRandomTeacher(), transform.position, Quaternion.identity);
        enemy.name = "Teacher";
        GameObject go = (GameObject)enemy.transform.Find("Root/Movement").gameObject;
        go.GetComponent<TeacherMovement>().PathContainer = PathContainer;
        GameManager.Instance.GameController.Startables.Add(go);
        Destroy(gameObject);
    }

}
