﻿using UnityEngine;
using System.Collections;
using SWS;
using DG.Tweening;

public class EnemyController : MonoBehaviour 
{
    public float Speed;
    public PathManager PathContainer { get; set; }
    public bool PathRandom { get; set; }

    private Animator _animator;
    private float _speed;
    private float _throw;
    private GameObject _gunTip;
    private splineMove _splineMove;
    

    public void Hit(int damage, string name)
    {
        _animator.Play("HitHead");
    }

    void OnEnable()
    {
        _animator = transform.Find("Kid").GetComponent<Animator>();
    }

    void StartLevel()
    {
        _gunTip = transform.FindChild("Tip").gameObject;

        _throw = 3 - GameManager.Instance.Level / 25;
        StartCoroutine(runThrow());

        D.Log("- PathContainer = {0}", PathContainer);

        if (PathContainer != null)
        {
            _splineMove = GetComponent<splineMove>();
            _splineMove.pathContainer = PathContainer;
            if (PathRandom)
            {
                _splineMove.loopType = splineMove.LoopType.random;
            }

            _splineMove.loopType = splineMove.LoopType.pingPong;
            _speed = Speed;
            _splineMove.speed = _speed;
            _splineMove.StartMove();

        }
        else
        {
            _speed = Speed;
            StartCoroutine(runMove());
        }
    }

    void EndLevel()
    {
        _splineMove.Stop();
        transform.DOMoveY(100, 10.0f);

    }

    private IEnumerator runThrow()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0, _throw));
            yield return StartCoroutine(throwFood());
        }
    }

    private IEnumerator runMove()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0, 2));
            yield return StartCoroutine(move());
        }
    }

    private IEnumerator move()
    {
        Vector3 newpos = GameManager.Instance.PlayerPosition;
        D.Log("- moving to {0}", newpos);
        while (Vector3.Distance(transform.position, newpos) > 1)
        {
            GetComponent<Rigidbody>().AddForce((newpos - transform.position) * ( Speed + GameManager.Instance.Level) * Time.deltaTime);
            yield return null;
        }
        yield return null;
    }

    private IEnumerator throwFood()
    {
        _animator.Play("Throw");
        yield return new WaitForSeconds(1);
        FoodObject food = Instantiate(GameManager.Instance.FoodFactory.GetRandom(), _gunTip.transform.position, transform.rotation) as FoodObject;
        food.OnCreated();
        food.OnFired();
        yield return null;
    }

}
