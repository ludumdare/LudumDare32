﻿using UnityEngine;
using System.Collections;

//  wait for player to press the fire button, spawn food
//  place on the facing controller
public class WeaponController : MonoBehaviour 
{
    public float Speed;

    public GameObject FoodPrefab{ get; set; }

    private GameObject _gunTip;

    void OnEnable()
    {
        GameManager.Instance.WeaponController = this;
    }

	// Use this for initialization
	void Start () {
        _gunTip = transform.FindChild("Tip").gameObject;
        FoodPrefab = null;
	}
	
	// Update is called once per frame
	void Update () {
        
	    if (Input.GetMouseButtonDown(0))
        {
            if (FoodPrefab != null)
            {
                GameObject food = (GameObject)Instantiate(FoodPrefab, _gunTip.transform.position, transform.rotation) as GameObject;
                food.GetComponent<FoodObject>().OnCreated();
                food.GetComponent<FoodObject>().OnFired();
            }
        }
	}
}
