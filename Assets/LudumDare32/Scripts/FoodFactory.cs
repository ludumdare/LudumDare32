﻿using UnityEngine;
using System.Collections;

public class FoodFactory : MonoBehaviour 
{

    public FoodObject FoodBurger;
    public FoodObject FoodFry;
    public FoodObject FoodPizza;
    public FoodObject FoodMilk;
    public FoodObject FoodBanana;
    public FoodObject FoodTaco;
    public FoodObject FoodOrange;

    private ArrayList _foodItems;

    void OnEnable()
    {
        GameManager.Instance.FoodFactory = this;

        _foodItems = new ArrayList();

        _foodItems.Add(FoodBanana);
        _foodItems.Add(FoodBurger);
        _foodItems.Add(FoodFry);
        _foodItems.Add(FoodMilk);
        _foodItems.Add(FoodTaco);
        _foodItems.Add(FoodPizza);
        _foodItems.Add(FoodOrange);
    }

    public FoodObject GetByName(string name)
    {
        if (name == "Burger")
        {
            GameManager.Instance.SoundController.PlayBurger();
            return FoodBurger;
        }

        if (name == "FrenchFry")
        {
            GameManager.Instance.SoundController.PlayFry();
            return FoodFry;
        }

        if (name == "Pizza")
        {
            GameManager.Instance.SoundController.PlayPizza();
            return FoodPizza;
        }

        if (name == "Milk")
        {
            GameManager.Instance.SoundController.PlayMilk();
            return FoodMilk;
        }

        if (name == "Banana")
        {
            GameManager.Instance.SoundController.PlayBanana();
            return FoodBanana;
        }

        if (name == "Taco")
        {
            GameManager.Instance.SoundController.PlayTaco();
            return FoodTaco;
        }

        if (name == "Orange")
        {
            GameManager.Instance.SoundController.PlayOrange();
            return FoodOrange;
        }

        return null;
    }

    public FoodObject GetRandom()
    {
        return (FoodObject)_foodItems[Random.Range(0, _foodItems.Count)];
    }

}
