﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{

    public GameObject Target;
    public float Distance;

    void OnEnable()
    {
        GameManager.Instance.CameraController = this;
    }

    void Start()
    {
        if (Target == null)
        {
            Target = GameObject.Find("Player/Root/Movement");
        }
    }

    void Update()
    {
        Vector3 np = new Vector3(Target.transform.position.x, transform.position.y, Target.transform.position.z - Distance);
        transform.position = np;
    }
}
