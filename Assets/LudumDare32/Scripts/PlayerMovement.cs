﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

//  use wasd to move the player
public class PlayerMovement : MonoBehaviour 
{
    public float            Speed;

    private Rigidbody       _rigidbody;
    private bool _active;

    void OnEnable()
    {
        _rigidbody = (Rigidbody)GetComponent<Rigidbody>();
        GameManager.Instance.GameController.Startables.Add(gameObject);
    }

	// Use this for initialization
	void StartLevel() {
        _active = true;
	}

    void EndLevel()
    {
        transform.DOMoveY(100, 10.0f);
        _active = false;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (!_active) return;

        D.Trace("{0}, {1}", Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * Speed * Time.deltaTime);
        transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * Speed * Time.deltaTime);
        GameManager.Instance.PlayerPosition = transform.position;

        //_rigidbody.AddForce(Vector3.forward * Input.GetAxis("Vertical") * Speed);
        //_rigidbody.AddForce(Vector3.right * Input.GetAxis("Horizontal") * Speed);
    }
}
