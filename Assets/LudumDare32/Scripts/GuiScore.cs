﻿using UnityEngine;
using System.Collections;

public class GuiScore : MonoBehaviour {

    public tk2dTextMesh textMesh;

	// Update is called once per frame
	void Update () {
        textMesh.text = GameManager.Instance.Score.ToString();
	}
}
