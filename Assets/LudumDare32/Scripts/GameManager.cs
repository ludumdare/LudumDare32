﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager>
{
    public CameraController CameraController { get; set; }
    public ScoreController ScoreController { get; set; }
    public WeaponController WeaponController { get; set; }
    public PrefabFactory PrefabFactory { get; set; }
    public CafeteriaController CafeteriaController { get; set; }
    public GameController GameController { get; set; }
    public EffectFactory EffectFactory { get; set; }
    public Vector3 PlayerPosition { get; set; }
    public FoodFactory FoodFactory { get; set; }
    public SoundController SoundController { get; set; }

    public int Level;
    public string Weapon;
    public int Score;
    public int Health;

    void OnEnable()
    {
        Level = 1;
        Score = 0;
        Health = 100;
        Weapon = "";
    }

}
