﻿using UnityEngine;
using System.Collections;

public class GuiWeapon : MonoBehaviour {

    public tk2dTextMesh textMesh;

    // Update is called once per frame
    void Update()
    {
        textMesh.text = GameManager.Instance.Weapon;
    }
}
