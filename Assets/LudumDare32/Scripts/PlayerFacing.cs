﻿using UnityEngine;
using System.Collections;

//  cast ray and rotate to face it
public class PlayerFacing : MonoBehaviour 
{
    private Camera          _camera;
    private Vector3         _worldposition;


    void OnEnable()
    {
        _camera = Camera.main;
    }

	// Use this for initialization
	void Start () {
	
	}

    // 	determine which cell has been touched
    void LateUpdate()
    {
        RaycastHit hit;
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 20, Color.gray);

        int layerMask = 1 << 10;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(ray.origin, ray.direction * hit.distance, Color.green);
            _worldposition = hit.point;
            Vector3 _lookPos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            transform.LookAt(_lookPos);
            //transform.rotation = Quaternion.LookRotation(Vector3.forward, _worldposition - transform.position);

        }
    }

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_worldposition, 0.25f);
    }
}
