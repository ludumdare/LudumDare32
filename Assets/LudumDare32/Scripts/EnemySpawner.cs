﻿using UnityEngine;
using System.Collections;
using SWS;

public class EnemySpawner : MonoBehaviour 
{
    public PathManager PathContainer;

    public void Spawn()
    {
        GameObject enemy = (GameObject)Instantiate(GameManager.Instance.PrefabFactory.GetRandomEnemy(), transform.position, Quaternion.identity);
        enemy.name = "Enemy";
        GameObject go = (GameObject)enemy.transform.Find("Root/Movement").gameObject;
        go.GetComponent<EnemyController>().PathContainer = PathContainer;
        GameManager.Instance.GameController.Startables.Add(go);
        Destroy(gameObject);
    }

}
