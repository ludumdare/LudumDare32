﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    public AudioClip soundBurger;
    public AudioClip soundTaco;
    public AudioClip soundFry;
    public AudioClip soundBanana;
    public AudioClip soundPizza;
    public AudioClip soundMilk;
    public AudioClip soundOrange;
    public AudioClip soundYes;
    public AudioClip soundNo;
    public AudioClip soundWahwah;
    public AudioClip soundOuch;
    public AudioClip soundWinner;
    public AudioClip soundLoser;
    public AudioClip soundTeacher;
    public AudioClip soundUgh;

    public void PlayBurger()
    {
        playSound(soundBurger);
    }

    public void PlayTaco()
    {
        playSound(soundTaco);
    }

    public void PlayFry()
    {
        playSound(soundFry);
    }

    public void PlayBanana()
    {
        playSound(soundBanana);
    }

    public void PlayPizza()
    {
        playSound(soundPizza);
    }

    public void PlayMilk()
    {
        playSound(soundMilk);
    }

    public void PlayOrange()
    {
        playSound(soundOrange);
    }

    public void PlayYes()
    {
        playSound(soundYes);
    }

    public void PlayNo()
    {
        playSound(soundNo);
    }

    public void PlayOuch()
    {
        playSound(soundOuch);
    }

    public void PlayWinner()
    {
        playSound(soundWinner);
    }

    public void PlayLoser()
    {
        playSound(soundLoser);
    }

    public void PlayTeacher()
    {
        playSound(soundTeacher);
    }

    public void PlayUgh()
    {
        playSound(soundUgh);
    }

    public void PlayWahwah()
    {
        playSound(soundWahwah);
    }


    private void playPitchSound(AudioClip clip)
    {
        playSound(clip, Random.Range(0.2f, 0.8f), 1.0f);
    }

    private void playSound(AudioClip clip)
    {

        D.Trace("[GameSounds] playSound");
        playSound(clip, 1.0f, 1.0f);
    }

    private void playSound(AudioClip clip, float pitch, float volume)
    {

        D.Trace("[GameSounds] playSound");

        GameObject go = new GameObject("Temp(Sound)");
        AudioSource gs = go.AddComponent<AudioSource>();
        gs.clip = clip;
        gs.volume = volume;
        gs.pitch = pitch;
        gs.Play();
        Destroy(go, clip.length + 0.1f);
    }

	void OnEnable () {
        GameManager.Instance.SoundController = this;
	}
	
}
