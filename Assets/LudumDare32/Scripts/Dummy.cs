﻿using UnityEngine;
using System.Collections;

public class Dummy : MonoBehaviour 
{

    void Start()
    {
        StartCoroutine(run());
    }

    private IEnumerator run()
    {
        yield return new WaitForSeconds(2);
        Animator ani = GetComponent<Animator>();
        //ani.playbackTime = 0.1f;
        ani.Play("HitHead");
        //ani.speed = 0.0f;
        //ani.StartPlayback();
    }

}
