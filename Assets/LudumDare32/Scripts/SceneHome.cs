﻿using UnityEngine;
using System.Collections;

public class SceneHome : MonoBehaviour {

    public SDN.AutoHide MenuPanel;
    public SDN.AutoHide LoserPanel;

    public void Yes()
    {
        GameManager.Instance.Level = 1;
        GameManager.Instance.Score = 0;
        GameManager.Instance.Health = 100;
        GameManager.Instance.Weapon = "";

        Application.LoadLevel("Load");
    }

    public void No()
    {
        LoserPanel.Show();
        MenuPanel.Hide();
        GameManager.Instance.SoundController.PlayWahwah();
    }

    public void Try()
    {
        LoserPanel.Hide();
        MenuPanel.Show();
        GameManager.Instance.SoundController.PlayWinner();
    }

    void Start()
    {
        MenuPanel.Show();
    }
}
