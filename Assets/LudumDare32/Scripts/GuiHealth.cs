﻿using UnityEngine;
using System.Collections;

public class GuiHealth : MonoBehaviour {

    public tk2dTextMesh textMesh;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        textMesh.text = "Health: " + GameManager.Instance.Health.ToString();
    }
}
