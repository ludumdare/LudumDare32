﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FoodSpawner : MonoBehaviour 
{
    private FoodObject _foodObject;
    void OnEnable()
    {
        transform.Find("Cube").gameObject.SetActive(false);
    }

    public void Spawn()
    {
        GameManager.Instance.GameController.Startables.Add(gameObject);
    }

    void StartLevel()
    {
        StartCoroutine(run());
    }

    private IEnumerator run()
    {
        while (true)
        {
            yield return StartCoroutine(changeFood());

            yield return new WaitForSeconds(15 + Random.Range(-5, 15));

        }
    }

    private IEnumerator changeFood()
    {
        _foodObject = (FoodObject)Instantiate(GameManager.Instance.FoodFactory.GetRandom(), transform.position, Quaternion.identity);
        _foodObject.transform.DOMoveY(3.0f, 1.0f).SetLoops(-1, LoopType.Yoyo);
        _foodObject.transform.DOScale(Vector3.one * 3.0f, 1.0f).SetLoops(-1, LoopType.Yoyo);
        yield return new WaitForSeconds(5);
        Destroy(_foodObject);
        yield return null;
    }

    void OnTriggerEnter(Collider other)
    {
        D.Log("- touched by {0}", other.tag);
        if (other.tag == "Player")
        {
            if (_foodObject != null)
            {
                GameManager.Instance.WeaponController.FoodPrefab = GameManager.Instance.FoodFactory.GetByName(_foodObject.Name).gameObject;
                _foodObject.transform.DOMoveY(20.0f, 3.0f);
            }
        }
    }

}
