﻿using UnityEngine;
using System.Collections;

public class GuiStartPanel : MonoBehaviour 
{
    public tk2dTextMesh LevelName;

	// Use this for initialization
	void Start () {
        LevelName.text = "Day " + GameManager.Instance.Level.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
