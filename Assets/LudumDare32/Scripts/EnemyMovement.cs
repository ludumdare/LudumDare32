﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour 
{
    public float Speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        transform.GetComponent<Rigidbody>().velocity = transform.forward * Speed;
	
	}
}
